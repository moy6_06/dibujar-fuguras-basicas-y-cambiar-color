package paint;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class Paint3 extends JFrame implements ActionListener {

    
    private JPanel contentPane;
    JButton boton []= new JButton [11];
    boolean l=false,cua=false,cir=false,tri=false,p=false;
    int x,y,n;
    
    public Paint3 (){
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 600);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        setBounds(20,20,800,600);  
        Botones();
        Raton r= new Raton();
        addMouseListener(r);
    }
    
       public static void main(String[] args) {
        Paint3 p=new Paint3();
        p.setVisible(true);
    }
    
    public void Botones(){
        
        for (int i = 0; i < 11; i++) {
            boton[i]= new JButton();
        }
       
        boton[0]= new JButton("Linea");        
        boton[1]= new JButton("Cuadrado");
        boton[2]= new JButton("Triangulo");
        boton[3]= new JButton("Estrella");
        boton[4]= new JButton("Circulo");
        
       //image = new ImageIcon("Diagonal.png");
      // icono =new  ImageIcon(image.getImage().getScaledInstance(boton[0].getWidth(), boton[0].getHeight(), Image.SCALE_DEFAULT ));
             
        boton[0].setBounds(10, 10, 100, 70);
        boton[1].setBounds(130, 10, 100, 70);
        boton[2].setBounds(250, 10, 100, 70);
        boton[3].setBounds(380, 10, 100, 70);
        boton[4].setBounds(510, 10, 100, 70);
        boton[5].setBounds(650, 10, 30, 30); // <<<<<<=============
        boton[6].setBounds(700, 10, 30, 30);
        boton[7].setBounds(750, 10, 30, 30);
        boton[8].setBounds(650, 60, 30, 30);
        boton[9].setBounds(700, 60, 30, 30);
        boton[10].setBounds(750, 60, 30, 30);
        
        boton[5].setBackground(Color.yellow);
        boton[6].setBackground(Color.BLACK);
        boton[7].setBackground(Color.CYAN);
        boton[8].setBackground(Color.GREEN);
        boton[9].setBackground(Color.LIGHT_GRAY);
        boton[10].setBackground(Color.MAGENTA);
        
        for (int o = 0; o < 11; o++) {
            add(boton[o]);
        }
        
        for (int u = 0; u < 11; u++) {
              boton[u].addActionListener(this);
        }
  
        
        
    }   
    
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==boton[0])
            l=true;
        
        if(ae.getSource()==boton[1])
            cua=true;
        
        if(ae.getSource()==boton[2])
            tri=true;
        
        if(ae.getSource()==boton[3])
            p=true;
        
        if(ae.getSource()==boton[4])
            cir=true;
        
        if(ae.getSource()==boton[5])
            n=1;
        
        if(ae.getSource()==boton[6] )
            n=2;
       
        if(ae.getSource()==boton[7])
            n=3;
        
        if(ae.getSource()==boton[8])
            n=4;
        
        if(ae.getSource()==boton[9])
            n=5;
        
        if(ae.getSource()==boton[10])
            n=6;
    
            
            
    }
    
    
    public class Raton implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent me) {
            x=me.getX();
            y=me.getY();
            
            if(l){                                
           Linea(contentPane.getGraphics(),x,y,x+50,y+100,n);
                l=false;
            }
            
            if(cua){
            Cuadrado(contentPane.getGraphics(),x-40,y-40 ,x+40,y-40,x+40,y+40,x-40,y+40,n );
                cua=false;
            }
            
            if(tri){
            Triangulo(contentPane.getGraphics(),x-30 ,y+30 ,x ,y-30 ,x+30 ,y+30,n );
                
                tri=false;
            }
             
            if(p){
            Estrella(contentPane.getGraphics(), x, y-40,/**/ x-10, y-10,/**/ x-40,y-10,/**/ x-20, y+20, /**/ x-30, y+50,/**/ x, y+20,/**/ x+30, y+50,/**/ x+20, y+20,/**/ x+40, y-10,/**/ x+10, y-10,/**/ x, y-40,n );            
                p=false;
            }
          
            if(cir){
            Circulo(contentPane.getGraphics(),x,y,n);
                cir=false;
            }
        }

        @Override
        public void mousePressed(MouseEvent me) {
            
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            
        }

        @Override
        public void mouseEntered(MouseEvent me) {
            
        }

        @Override
        public void mouseExited(MouseEvent me) {
            
        }
        
        
    }//   raton     
    
    
    public void Linea (Graphics g, int x, int y, int x1, int y1, int c)
    {   
           // g.fillRect(x, y, x1, y1);
          
            
             switch(c){
            case 1:
                 g.setColor(Color.yellow);
                   g.drawLine(x, y, x1, y1);       
               break;
            case 2:
                g.setColor(Color.black);  
                g.drawLine(x, y, x1, y1);
                break;
            case 3:
                g.setColor(Color.cyan); 
                g.drawLine(x, y, x1, y1);
                break;
            case 4:
                g.setColor(Color.GREEN);  
                g.drawLine(x, y, x1, y1);
                break;
            case 5:
                g.setColor(Color.gray);  
                g.drawLine(x, y, x1, y1);
                break;
            case 6: 
                g.setColor(Color.pink); 
                g.drawLine(x, y, x1, y1);
                break;
            default:
       }
         
    }
    
    
    public void Cuadrado(Graphics g,int x, int y, int x1, int y1, int x2, int y2,int x3,int y3, int c  ){
            
       switch(c){
            case 1:
                 g.setColor(Color.yellow);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
               break;
            case 2:
                g.setColor(Color.black);
                g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
                break;
            case 3:
                g.setColor(Color.cyan);
                g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
                break;
            case 4:
                g.setColor(Color.GREEN);
                g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
                break;
            case 5:
                g.setColor(Color.gray);
                g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
                break;
            case 6: 
                g.setColor(Color.pink); 
                g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2);
                 g.drawLine(x2, y2, x3, y3);
                 g.drawLine(x3, y3, x, y);
                break;
            default:
       }
       
      }
    
    public void Triangulo(Graphics g, int x , int y,int x1, int y1,int x2, int y2, int c){
     
        
      switch(c){
            case 1:
                 g.setColor(Color.yellow);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y);
               break;
            case 2:
                 g.setColor(Color.black); 
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y); 
                break;
            case 3:
                g.setColor(Color.cyan);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y);    
                break;
            case 4:
                g.setColor(Color.GREEN);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y);  
                break;
            case 5:
                g.setColor(Color.gray);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y);   
                break;
            case 6: 
                g.setColor(Color.pink);
                 g.drawLine(x, y, x1, y1);
                 g.drawLine(x1, y1, x2, y2); 
                 g.drawLine(x2, y2, x, y);  
                break;
            default:
       }
     
    }
    

    public void Estrella(Graphics g, int x ,int y, int x1, int y1, int x2 ,int y2, int x3, int y3, int x4 ,int y4, int x5, int y5,int x6 ,int y6, int x7, int y7, int x8 ,int y8, int x9, int y9, int x10, int y10,int c ){
        
   
       
       switch(c){
            case 1:
                g.setColor(Color.yellow);
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
               break;
            case 2:
                g.setColor(Color.black);   
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
               break;
            case 3:
                g.setColor(Color.cyan);    
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
                break;
            case 4: 
                g.setColor(Color.GREEN);  
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
                break;
            case 5:
                g.setColor(Color.gray);  
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
                break;
            case 6:
                g.setColor(Color.pink);  
                g.drawLine(x, y, x1, y1);
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.drawLine(x3, y3, x4, y4);
       
                g.drawLine(x4, y4, x5, y5);
                g.drawLine(x5, y5, x6, y6);
                g.drawLine(x6, y6, x7, y7);
                g.drawLine(x7, y7, x8, y8);
                g.drawLine(x8, y8, x9, y9);
                g.drawLine(x9, y9, x10,y10);
                break;
               
           
           
       }
        
    }
    
    public void Circulo(Graphics g, int x, int y,int c){
        Graphics2D circulo=(Graphics2D)g;
        
        switch(c){
            case 1:
                   circulo.setColor(Color.yellow);
                   circulo.setStroke(new BasicStroke(10.f) );
                   circulo.fillOval(x, y, 60, 60);
                break;
            case 2:
                circulo.setColor(Color.black);
                circulo.setStroke(new BasicStroke(10.f) );
                circulo.fillOval(x, y, 60, 60);
                break;
            case 3:
                circulo.setColor(Color.cyan);
                circulo.setStroke(new BasicStroke(10.f) );
                circulo.fillOval(x, y, 60, 60);
                break;
            case 4:
                circulo.setColor(Color.GREEN);
                circulo.setStroke(new BasicStroke(10.f) );
                circulo.fillOval(x, y, 60, 60);
                break;
            case 5:
                circulo.setColor(Color.gray);
                circulo.setStroke(new BasicStroke(10.f) );
                circulo.fillOval(x, y, 60, 60);
                break;
            case 6:
      
          circulo.setColor(Color.pink);
                circulo.setStroke(new BasicStroke(10.f) );
                circulo.fillOval(x, y, 60, 60);
                break;
            default:
        }
        
    }
    
}
 